<?php

    class BITM
    {

        public $window;
        public $door;
        public $chair;
        public $table;
        public $whiteboard;

        public function coolTheAir()
        {
            echo "I'm cooling the air";
        }

        public function compute()
        {
            echo "I'm computing";
        }

       public function show()
        {
            //  echo "I'm showing";
            echo $this->window . "<br>";
            echo $this->door . "<br>";
            echo $this->table . "<br>";
            echo $this->chair . "<br>";
            echo $this->whiteboard . "<br>";
        }
        public function setAll()
        {
             $this->setWindow("I'm a window");
             $this->setChair("I'm a chair");
             $this->setDoor("I'm a door");
             $this->setTable("I'm a table");
             $this->setWhiteboard("I'm a whiteboard");
        }

        public function setWindow($window)
        {
            $this->window = $window;
        }

        public function setChair($chair)
        {
            $this->chair = $chair;
        }

        public function setDoor($door)
        {
            $this->door = $door;
        }

        public function setTable($tab)
        {
            $this->table = $tab;
        }
        public function setWhiteboard($board){
            $this->whiteboard = $board;
        }
    }//end of class BITM

    $obj_BITM_at_ctg = new BITM;

    $obj_BITM_at_ctg->setDoor("I'm a door");
    $obj_BITM_at_ctg->setTable("I'm a table");
    $obj_BITM_at_ctg->setWindow("I'm a window");
    $obj_BITM_at_ctg->setWhiteboard("I'm a whiteboard");

    $obj_BITM_at_ctg->show();

    Class BITM_Lab402 extends BITM{

    }
   /* $objBITM_Lab = new BITM_Lab402();
    $objBITM_Lab->setAll();
    $objBITM_Lab->show();*/
$obj_BITM_Lab = new BITM_Lab402;

$obj_BITM_at_ctg->setDoor("this is a door");
$obj_BITM_at_ctg->setTable("this is a table");
$obj_BITM_at_ctg->setWindow("this is a window");
$obj_BITM_at_ctg->setWhiteboard("this is a whiteboard");

?>