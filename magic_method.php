<?php

class AnyClass{

    public $myProperty1;
    public $myProperty2;
    public $otherProperty1;

   public function __sleep()
    {
        echo "I'm inside sleep magic method";
        return array ("myProperty1","myProperty2");
    }
    public function __wakeup(){
        echo "I'm inside wake up magic method";

    }
}
    $obj=new AnyClass();
    $serializeString= serialize($obj);
    echo $serializeString. "<br>";
    $arr=unserialize($serializeString);
    echo "<br>";
    print_r($arr);
?>